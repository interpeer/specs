---
###
# Internet-Draft Markdown Template
#
# Rename this file from draft-todo-yourname-protocol.md to get started.
# Draft name format is "draft-<yourname>-<workgroup>-<name>.md".
#
# For initial setup, you only need to edit the first block of fields.
# Only "title" needs to be changed; delete "abbrev" if your title is short.
# Any other content can be edited, but be careful not to introduce errors.
# Some fields will be set automatically during setup if they are unchanged.
#
# Don't include "-00" or "-latest" in the filename.
# Labels in the form draft-<yourname>-<workgroup>-<name>-latest are used by
# the tools to refer to the current version; see "docname" for example.
#
# This template uses kramdown-rfc: https://github.com/cabo/kramdown-rfc
# You can replace the entire file if you prefer a different format.
# Change the file extension to match the format (.xml for XML, etc...)
#
###

coding: utf-8
title: "Capabilities for Distributed Authorization"
abbrev: "Capabilities for Distributed Authorization"
docname: draft-jfinkhaeuser-caps-for-distributed-auth-02
category: info
submissiontype: independent
ipr: interpeer
date: 2023-11-01
stand_alone: yes
pi:
- toc
- sortrefs
- symrefs
- docmapping
- comments # TODO remove comments

number:
v: 3
# TODO
# consensus: true XXX not for 'independent'
# area: AREA
workgroup: Interpeer Project
keyword:
 - capabilities
 - authorization
 - authentication
 - power of attorney
 - cryptography

author:
 -
  ins: J. Finkhaeuser
  fullname: Jens Finkhäuser
  asciiFullname: Jens Finkhaeuser
  org: Interpeer gUG (haftungsbeschraenkt)
  abbrev: Interpeer
  # TODO add mailing address
  email: ietf@interpeer.io
  uri: https://interpeer.io/
 -
  ins: S. D. Penna
  fullname: Sérgio Duarte Penna
  asciiFullname: Sergio Duarte Penna
  org: Instituto Superior de Engenharia do Porto
  abbrev: ISEP
  street: Rua Dr. António Bernardino de Almeida, 431
  code: 4249-015
  city: Porto
  country: Portugal
  email: sdp@isep.ipp.pt
  uri: https://isep.ipp.pt/

normative:
  NIST.IR.8366: DOI.10.6028/NIST.IR.8366
  RM3420: DOI.10.7249/RM3420
  BCP72: RFC3552

informative:
  I-D.draft-irtf-hrpc-guidelines-20:
  ISOC-FOUNDATION:
    title: "Internet Society Foundation"
    author:
      org: "Internet Society Foundation"
    target: https://www.isocfoundation.org/
  X.509: ITU.X509.2000
  OCAP: DOI.10.1145/365230.365252
  ICAP: DOI.10.1109/SECPRI.1989.36277
  ADACORSA:
    title: Airborne data collection on resilient system architectures
    date: 2020-05-01
    target: https://www.kdt-ju.europa.eu/projects/adacorsa
  RDF:
    title: "RDF 1.1 Concepts and Abstract Syntax"
    date: 2014-02-25
    author:
      org: RDF Working Group of the World Wide Web Consortium (W3C)
    target: https://www.w3.org/TR/rdf11-concepts/
  WIREGUARD:
    title: "WireGuard: Next Generation Kernel Network Tunnel"
    date: 2020-06-01
    author:
      fullname: Jason A. Donenfeld
      ins: J. A. Donenfeld
    target: https://www.wireguard.com/papers/wireguard.pdf
  UCAN:
    title: "User Controlled Authorization Network"
    author:
      org:  "UCAN Working Group"
    target: https://ucan.xyz/
  ZCAP-LD:
    title: "Authorization Capabilities for Linked Data v0.3"
    date: 2023-01-22
    author:
    - fullname: Christine Lemmer-Webber
    - fullname: Manu Sporny
    - fullname: Mark S. Miller
      ins: M. S. Miller
    target: https://w3c-ccg.github.io/zcap-spec/
  POA-IOT: DOI.10.1109/JIOT.2023.3265407
  MACAROONS:
    title: "Macaroons: Cookies with Contextual Caveats for Decentralized Authorization in the Cloud"
    author:
    - fullname: Arnar Birgisson
    - fullname: Joe Gibbs Politz
    - fullname: Úlfar Erlingsson
    - fullname: Ankur Taly
    - fullname: Michael Vrable
    - fullname: Mark Lentczner
    date: 2014
    target: https://research.google/pubs/pub41892/
  CAPBAC: DOI.10.1016/j.mcm.2013.02.006





--- abstract

Authorization is often the last remaining centralized function in a distributed
system. Advances in compute capabilities of miniaturized CPUs make alternative
cryptographic approaches feasible that did not find such use when first
envisioned. This document describes the elements of such cryptographically backed
distributed authorization schemes as a reference for implementations.

--- middle

[^fixme]: FIXME

# Introduction {#sec:intro}

In 1964, Paul Baran at the RAND Corporation described centralized, decentralized
and distributed communications networks and their properties {{RM3420}}. Baran's
argument was that because in distributed systems, each node can reach many other
nodes, failure of a single node need not impact the ability of other nodes to
communicate.

This resilience is desirable in distributed systems today. Therefore it seems
an oversight that authentication and authorization in modern system is often a
centralized function.

This document explores previous attempts at distributed authorization schemes,
and outlines common elements of such solutions in order to provide a reference
for future work.

# Conventions and Definitions {#sec:intro-conventions}

{::boilerplate bcp14-tagged}

In order to respect inclusive language guidelines from {{NIST.IR.8366}} and
{{?I-D.draft-knodel-terminology-13}}, this document uses plural pronouns.

# Use Cases {#sec:use-cases}

If resilience is a desirable feature in distributed systems as Baran suggested,
and a system is the sum of its functions, it follows that all of a system's
functions must be resilient to failure individually to achieve resilience in
the overall system.

The following explores use cases in which reliance on a centralized
authorization negatively impacts system resilience, even when the remaining
system is sufficiently resilient in principle.

## IoT On-boarding {#sec:uc-iot}

On-boarding IoT devices into an overall system requires authentication and
authorization; this may need to be mutual.

In such scenarios, new devices rarely have connectivity before completing
the on-boarding process. It follows that authentication and authorization must
work in a fully offline fashion, which in turn requires that authorization tokens
provided to the device contain all information required for the authorization
step.

This specific problem is also addressed in {{POA-IOT}} and related work.

## UAV Control Handover {#sec:uc-uav}

A similar argument applies to control handover of unmanned aerial vehicles
(UAV). The concept of Beyond Visual Line of Sight (BVLOS) missions is to send
drones into places that are harder or more costly to reach for humans.

Control handover refers to transferring operational control for a drone from one
ground control station to (GCS) another. Control handover bears similarities to IoT
on-boarding in that the drone is on-boarded to a new control system (and the
previous system relinquishes control).

In general, aviation authorities such as FAA, EASA, etc. expect control handover
to occur under ideal circumstances, in which centralized authorization schemes
suffice because connectivity is guaranteed. There are, however, classes of
scenarios where this cannot be expected.

### Remote Location {#sec:uc-uav-remote}

In order to guarantee BVLOS operations in very remote locations, research
projects such as {{ADACORSA}} assume use cases in which ground control stations
hand over control of a UAV to each other. In remote locations, two ground control
stations between which handover occurs may not have direct connectivity to
each other, nor indirect connectivity through the internet.

In such cases, it is necessary for the UAV to act as trustworthy transmission
channel, by storing and forwarding authorization information.

### Emergency Response {#sec:uc-uav-emergency}

Emergency response teams may require UAVs in the vicinity to immediately clear
the airspace and go to ground. This effectively translates to the emergency
response team operating a ground control station that forcibly takes over control
and issues a single command.

As emergency responses are, by definition, typically required in situations
where normal operations cannot be assumed, this must include prerequisites for
dealing with absent connectivity. Such an emergency control handover must be
possible even when connectivity is absent.

### Mobile Ground Control Stations {#sec:uc-uav-mobile-gcs}

A comparable scenario to the above describes situations in which UAV attach
to a mobile ground control station. Specific scenarios may range from cave
exploration to investigating burning buildings.

The commonality here is that the UAV cannot establish connectivity to a wider
system, but can connect to the mobile GCS. This in turn may act as a
communications relay to the outside world, but may be too limited in capacity
to permit online, centralized authorization.

## Zero Round-Trip-Time (0-RTT) {#sec:uc-0-rtt}

If fast authorization is a goal, reducing the number of roundtrips to establish
a privilege follows. Reducing this to 0-RTT implies being able to store and send
verifiable authorization data, rather than engaging in a more complex handshake.

Of course, authorization can only follow when authentication already occurred.
Authentication in a 0-RTT protocol is predicated on prior key exchange and
verification.

Both {{WIREGUARD}} and DTLS 1.3 {{?RFC9147}} offer 0-RTT handshakes. In the
former, keys are pre-shared out of band, because WireGuard is used to establish
static VPN tunnels. Because mutual authentication is assumed to be part of this
process, authenticated encryption is sufficient to ensure that the keys are
safely associated with network addresses in a 0-RTT roundtrip.

By contrast, DTLS simply offers different kinds of handshakes. 0-RTT can only
be used for reconnection when a previous full handshake has provided sufficient
authentication.

In either case, when 0-RTT authentication is offered, 0-RTT authorization should
also be offered in order not to introduce more latency at this latter stage.

## Bike Rental {#sec:uc-bike-rental}

Standing in for similar schemes, urban bike rental is a candidate for
distributed authorization.

As a user, you typically use a mobile application to purchase some usage
time via an internet enabled service. You then use the application to open
a lock on a candidate bike, use it, and lock it again on return.

But the authorization to use bikes from the service does not expire with the
return of a bike. It is a feasible and supported use case to lock one bike
e.g. to go into a shop, then find a new bike to return the shopping home, all
within the same booked time slot.

If centralized authorization systems are used, each of these unlocking
operations will require internet connectivity. If connectivity is not given,
access to the service will be denied, even though contractually speaking, it
must be given.

## Principle of Least Authority (POLA) {#sec:uc-pola}

The Principle of Least Authority refers to a design method for authorization
systems in which a process is given only the authority required to perform
its purpose, and no more.

This is in contrast to identity-based authorization systems, in which processes
can perform any activity that an identified user is permitted to perform. More
advanced systems such as Role-Based Access Control (RBAC) function similar to
identity-based authorization, except that each identified user can hold
multiple roles, and roles are typically more strictly limited.

On a spectrum where identity-based authorization exists on one end, and RBAC
occupies some middle ground, POLA exists on the opposite end of identity-based
authorization.

Capabilities are eminently suitable for use with POLA, because they can encode
arbitrarily fine-grained access control data.

In principle, POLA is very similar to how modern mobile operating systems
encode permissions: each app must explicitly request e.g. access to the camera,
internet, file system, etc. However, where such permissions are applied only
locally, capabilities can be transmitted to be applied on remote nodes. In
effect, a local application can be granted access to a networked, rather
than merely a local camera.

## Human Rights Considerations {#sec:uc-hrpc}

{{!RFC8280}}, updated by {{I-D.draft-irtf-hrpc-guidelines-20}}, lists a number of
distinct objectives that help support human rights in protocol design. The above
distributed authorization scheme addresses a number of them, such as Connectivity,
Reliability, Content agnosticism, Integrity, Authenticity, Pseudonymity, Censorship
Resistance, Outcome Transparency, Adaptability, Decentralization and Security, and
by way of producing this document, Open Standards.

Rather than address each in detail, suffice to say that the use of pseudonymous
public keys, and proofs based on cryptographic signatures, the majority of these
objectives are reached.

It remains to highlight that the scheme outlined in this document observes the
end-to-end principle, precisely by temporally decoupling different concerns. This
permits for almost arbitrarily disrupted connectivity, and thus also censorship
resistance. As capabilities can travel entirely out-of-band to any resource data,
e.g. by sneakernet or similar means, they can be a building block of protocols
that provide better human rights protections than systems that rely on temporal
coupling of authorization concerns.

# Problem Space {#sec:problem-space}

As outlined in the above use-cases, distributed authorization is not a goal in
itself, but may be desirable in a distributed system.

In many systems, authentication and authorization are intrinsically linked.
Logging in with a user name and password is one such example. Providing the
correct password proves that the person at the keyboard is authorized to access
a resource. But at the same time, providing the correct password in combination
with a user name authenticates this user. Furthermore, any permissions granted
to the user are typically linked to the user name, as that remains stable
throughout password changes.

Since authorization is closely related to authentication, it is worth exploring
briefly how authentication has dealt with distributing its operations.

## Authentication {#sec:authentication}

Password-based authentication mechanisms require that the tuple of user name and
password (or password hash) are sent to some central repository where records of
such tuples are kept; if the tuple is found, the user name is authenticated.

This common scheme mixes different aspects to authentication, however, which are
worth disambiguating.

Identification:
: (((!identification))) The act of establishing the *attributes* that describe
a *person* in sufficient detail to identify them.

Endowment:
: (((!endowment))) The act of associating attributes derived during the
*identification* phase with an *identifier* such as may be used in *secret
proving*.

Secret Proving:
: (((!secret proving))) Logging in to a system requires proving that one is in
possession of some *secret*. It usually also requires providing some
*identifier* that the secret is associated with -- this permits tying secret
proving back to *identification* and *endowment* steps.

This distinction becomes somewhat more relevant when we move towards distributed
authentication schemes, which rely on public key cryptography. It's worth noting
that it is possible to construct systems that rely solely on secret proving,
even without identifiers of any sort.

What is typically understood as "authentication", however, is a combination of
secret proving and endowment. Specifically, once the secret is proven, a user
database is consulted to endow the identifier with associated attributes (i.e.
a user profile).

In particular, identification occurs in a preparation phase, according to the
system's policies -- sometimes prior to when an identifier is first associated
with a secret, sometimes after that event, and often before access to the
system's functions is granted.

~~~ aasvg
    I. Preparation             II. Real-Time

  +----------------+
  | Identification |
  +----------------+

           --- arbitrarily long delay ---

                              +----------------+
                              | Secret Proving |
                              +----------------+

                              +-----------+
                              | Endowment |
                              +-----------+
~~~
{:
  artwork-svg-options="--spaces=2"
  title="Phases in Password based Authentication Schemes"
}

Note that a finer grained model is possible in which we distinguish between
when identifiers are endowed with attributes *in principle* vs. *for use of
the system*, but that is unlikely to clarify this document. We prefer a
simpler model here.

### Web of Trust {#sec:authentication-wot}

In Web of Trust based systems, starting with Philip R. Zimmermann's Pretty Good
Privacy (PGP), public keys are exchanged with some metadata attached. This
performs some part of endowment (((endowment))) in that it provides
the link between a public key and a user identifier (see
{{?RFC4880, Section 11.1}}).

Other parts such as identification are not specified. These often consist of
manual checks that the user identifier belongs to some person holding the
corresponding private key, and may involve verifying of government issued
identification documents. Once such a check is passed, the verifier issues a
digital signature over the tuple of user identifier and public key to provide
some proof that identification has occurred.

Endowment in Web of Trust occurs when a sufficient number of sufficiently
trustworthy signatures have been reached. The precise number of signatures and
trust levels to be deemed sufficient is in the control of the recipient of
transferable public key packets, however.

In either case, the major distinction compared to passwowrd based authentication
schemes is that endowment is shifted from the real-time to the preparation phase.

~~~ aasvg
    I. Preparation             II. Real-Time

  +----------------+
  | Identification |
  +----------------+

  +-----------+
  | Endowment |
  +-----------+

           --- arbitrarily long delay ---

                              +----------------+
                              | Secret Proving |
                              +----------------+
~~~
{:
  artwork-svg-options="--spaces=2"
  title="Phases in Public Key based Authentication Schemes"
}

This shift alone is successful in removing the reliance on some user database
in the real-time phase.

### TLS Certificates {#sec:authentication-tls}

A similar concept is applied in TLS {{?RFC8446}}, where {{?X.509}} certificates
are used for endowment.

The major difference to Web of Trust based systems is how trust is established.
Instead of relying on a recipient defined method of determining trust,
certificates are issued by one of a set of well-known trust sources. Information
on these is stored in root certificates, which are distributed to the machines
participating in the system.

While there are globally issued root certificates for entities that perform
endowment professionally, it is always possible for a system designer to
include other root certificates.

Other than this, {{?X.509}} certificates behave like the web of trust in that
endowment is shifted to the preparation phase.

### Secret Proving {#sec:authentication-secret}

Neither {{?X.509}} certificates nor the transferable public key packets in
{{?RFC4880}} provide any means for secret proving. This is left to other parts
of TLS or PGP.

In TLS, the handshake during connection establishment is used to send challenges
that only machines with the correct private key can respond to. PGP, which aims
to provide privacy at rest, simply encrypts content with a secret key which is
then encrypted with the recipient's public key. Other parties cannot decrypt this,
which keeps content safe.

TLS and PGP are not the only public key cryptography based authentication systems,
but they can stand in for the two most common classes of such systems: one aims
to establish trust from authoritative sources. The other aims to establish trust
based on the trust requirements of the recipient.

Both systems also strictly speaking separate endowment from secret proving.
While in TLS the certificates are transmitted as part of the overall handshake,
creating certificates nevertheless occurs beforehand. This temporal decoupling
(((!temporal decoupling))) is a key property that may also be applied to
authorization.

## Authorization {#sec:authorization}

Authorization occurs only after secret proving. Once an identity has been
established, it is then mapped to associated privileges, which determine which
object(s) it has access to.

There exist a plethora of methods to establish this mapping. Access-control
lists (ACL) simply provide tuples of identities, privileges and associated
objects. Role-based access control (RBAC) is effectively identical, if the
identities specified are not those of individuals, but of groups (as a group
member, an individual inhabits the associated role). A comparable approach is
Organization-based access control (OrBAC), which not only abstracts the
identity to that of a role, but performs a similar abstraction on the object
and privilege.

More complex systems such as context- or lattice-based access control (CBAC and
LBAC respectively) derive a mapping from properties of or labels attached to the
individuals and objects. Finally, graph-based access control (GBAC) starts with
a graph of an organization, and derives privileges from the properties inherited
by being part of a larger organizational structure.

What these systems address is the problem of *managing* the mapping of an
identity to access privileges for objects, where each system has advantages and
disadvantages for various use cases.

In the abstract, however, they each operate on the following pieces of
information:

Subject:
: The subject (((!subject))) is the identity (individual or group/role) that
intends to perform an action.

Action:
: The action (((!action))) the subject intends to perform may be as simple as
reading or writing a resource, but can be more complex.

Object:
: Actions are performed on objects (((!object))), such as a file or network
resource.

Request Tuple:
: A request tuple (((!request tuple))) consists of the subject, action and
(optional) object.

Privilege:
: A privilege (((!privilege))) encodes whether or not an action is permitted.

Authorization Tuple:
: An authorization tuple (((!authorization tuple))) encodes system state, and
is thus a tuple of a subject, a privilege and an (optional) object.

The act of authorization translates from a request tuple to a Boolean response
determining whether a request is permitted. Similar to authentication above
({{sec:authentication}}), we can subdivide this into distinct steps that occur
in phases.

Authorization Management:
: The first phase is authorization management (((!authorization management))),
in which an *identifier* is associated with *authorization tuples* according to
the management scheme in use.

Authorization Query:
: In the authorization query (((!authorization query))) phase, a
*request tuple* is used to look up in some store whether the request yields
a positive or negative response. Conceptually, this resolves a *request tuple*
into an *authorization tuple* -- or lack thereof. This effectively yields a
boolean response to the request.

Access Granting:
: The final stage is to grant access based on whether the *authorization
query* step succeeded or failed.

~~~ aasvg
    I. Preparation             II. Real-Time

  +--------------------------+
  | Authorization Management |
  +--------------------------+

           --- arbitrarily long delay ---

                              +---------------------+
                              | Authorization Query |
                              +---------------------+

                              +-----------------+
                              | Access Granting |
                              +-----------------+

~~~
{:
  artwork-svg-options="--spaces=2"
  title="Phases in traditional Authorization"
}

Management of authorization always occurs in a preparation phase. Upon request
for access to a resource, systems perform authorization query, immediately
followed by access granting. The conceptual translation from request tuple to
authorization tuple is an implementation detail, and typically remains conceptual
at best.

By contrast, distributed authorization instead deals in authorization tuples,
which can be stored and distributed out-of-band. Because of this, they can encode
the least authority (see POLA, {{sec:uc-pola}}), independent of which mapping
system was chosen to manage authorization policies.

It may be of interest that and authorization tuple is semantically equivalent
to an RDF triple ({{RDF}}), in that it encodes a specific relationship between a
subject and an object. Authorization tuples that consists solely of IRIs
{{?RFC3987}} are also syntactically RDF triples. This implies that
authorization tuples can encode arbitrarily complex authorization information by
building the knowledge graph resulting from resolving such an RDF triple.

### Single Point of Failure {#sec:authorization-spof}

The previous section on different access control methods should illustrate
sufficiently that authorization management is a complex problem; complex enough for
multiple competing management methods to emerge.

Faced with such a complex problem, it is no surprise that solutions tend to
bring this function to a centralized location. Permitting an authority to
manage this complexity in a single place is of course simpler than managing it
across multiple locations.

It is tempting to then couple this authorization management function with the
access granting (((access granting))) function. The downside of this is that it
also centralizes the access granting function, creating a single point of failure.

Particularly vulnerable to this single point of failure
are private systems in which all access is controlled by specific privileges. Systems
with publicly available parts may still provide those functions that do not rely
on any privileges.

~~~ aasvg
      +---------------+             +---------------+
      | Application A |             | Application B |
      +-----------+---+             +----+----------+
                  |                      |
             .----)----------------------)---.
            |     v                      v   |
            |  +--------------------------+  |
            |  |     Access Granting      |  |
            |  +--------------------------+  |
            |               ^                |
            |               |                |
            |               v                |
            |  +--------------------------+  |
            |  | Authorization Management |  |
            |  +--------------------------+  |
            |               ^                |
            '---------------)---------------'
                            |
                       +----+----+
                       | Grantor |
                       +---------+
~~~
{:
  artwork-svg-options="--spaces=2"
  title="Single Point of Failure due to the complexity of Authorization Management"
}

### Temporal Coupling {#sec:authorization-temp}

The other class of problems with centralized authorization relate to the
temporal coupling of granting access (((!access granting))) and resolving
authorization queries (((!authorization query))). The abstract request
introduced above of resolving a request tuple (((request tuple))) to a
Boolean response tightly couples both steps.

It may be beneficial to disambiguate between participants in such a system.

From the perspective of the person operating the access control management
system, granting access occurs whenever they make an entry into an
authorization database.

The machine permitting an authorized user to perform an action, however, grants
or denies access in the moment the action is requested. If this second form
of access granting (((access granting))) is based on a real-time authorization query,
it couples granting access to such a query in the temporal dimension.

The key insight into distributing authorization effectively is that it has
little to do with managing access control databases. Instead, it explicitly
temporally decouples the authorization query from granting access.

~~~ aasvg
        +-------------+
        | Application |
        +------+------+
               ^
               | "May I do X?"
   .-----------)---------------------.
  |            |                     |
  |            v                     |
  |   +-----------------+            |
  |   | Access Granting |            |
  |   +--------+--------+            |
  |            ^                     |
  |            | authorization query |
  |            v                     |
  |     .-------------.              |
  |    |               |             |
  |    |'-------------'|             |    +---------+
  |    | authorization |<------------)----| Grantor |
  |    |  tuple store  |             |    +---------+
  |     '-------------'              |
  '---------------------------------'
~~~
{:
  artwork-svg-options="--spaces=2"
  title="Temporal Coupling due to bundling Access Granting with Authorization Queries"
}

# Previous Work {#sec:prior}

Dividing the authentication problem into endowment and secret proving helps
illustrate how web of trust systems introduce temporal decoupling between these
functions, in a way that e.g. TLS does not.

In much the same way, dividing the authorization problem into querying an
authorization database and granting access to an object suggests that
authorization, too, can be temporally decoupled.

This section lists prior work where some temporal decoupling of this kind has
been performed.

## Object-Capabilities (OCAP) {#sec:prior-ocap}

Dennis and Van Horn described an approach for securing computations in
"multiprogrammed" systems in 1965/66 ({{OCAP}}). The context in which they operated had
little to do with modern distributed systems.

However, they recognized the trend of running computing systems complex enough
that multiple programmers would contribute to its overall function. This raised
a desire for access control to individual sub-functions, which a security kernel
within the operating system was to provide.

The key differentiator to other systems was that in OCAP, a calling process was
to present a "capability" (((!capability))), a serialized token to the process
being invoked. This capability was intended to encode all relevant information
the called process would need to determine whether the caller was permitted to
perform such an action.

These properties of being serializable and containing all relevant authorization
information imply that, conceptually, capabilities are cached results of an
authorization query (((authorization query))). The called process can then perform
access granting (((access granting))) without issuing such a query itself, thereby
temporally decoupling the two functions.

## Identity-Capabilities (ICAP) {#sec:prior-icap}

The OCAP system proved to have a particular weakness, namely that "the right to
exercise access carries with it the right to grant access". This is the result
the information encoded in an OCAP capability: it contains a reference to the
object and action to perform, but does not tie this to any identity.

In 1988, Li Gong sought to address this with an Identity-Capability model
({{ICAP}}). Including an identity in the capability token arrives at the
authorization tuple (((authorization tuple))) in {{sec:authorization}}.

Furthermore, ICAP introduces the notion of capability use in networked systems.
ICAP does this by temporally decoupling the authorization query from access granting.

The main criticism levelled against the paper and capability-based approaches in
general in the following years was that some functions were missing, such as a
check for revocations. Proposals to address this often added centralized
functions again, which led to criticism of the distributed approach in general.

## Pretty Good Privacy {#sec:prior-pgp}

While we previously discussed PGP in terms of authentication in
{{sec:authentication-wot}}, a key property of PGP is the introduction of trust
signatures ({{?RFC4880, Section 5.2.3.13}}).

Trust signatures do not merely authenticate a user, they introduce a kind of
authorization as well, as they carry specific notions for what the provided
public key may be trusted for. The trust signature thus encodes specific kinds
of privileges (((privilege))) of an authorization tuple (((authorization tuple))),
while the public key encodes a subject (((subject))). The only component
missing in the tuple is the object (((object))).

While the authorization tuple in PGP is incomplete, the system is based on
public key cryptography, and can thus be used to securely verify a binding
between the tuple elements.

## JSON Web Tokens (JWT) {#sec:prior-jwt}

JSON Web Tokens ({{?RFC7519}}) provide a standardized way for serializing
access tokens. Current uses are in systems with centralized authorization
functions such as OAuth ({{?RFC6749}}).

However, the fundamental notion of capabilities, that a serializable token
carries authorization information, is provided also here. Furthermore, JWT
combines this with cryptographic signatures, providing for - in theory -
temporal decoupling as previously discussed.

It's well worth pointing out that JWT is suitable as a portable, modern
capability format - all it requires is to encode all necessary information
within its fields. One serialization format in JWT for this is {{UCAN}}.

## ZCAP-LD {#sec:prior-zcap}

Aimed at the linked data space, {{ZCAP-LD}} is an expression of cryptographic
capabilities for authorization that relies heavily on linked data. While
conceptually, the specification shares many similarities with the capability
concept in this document, the use of linked data can lead to systems that
do not provide for temporal decoupling (((temporal decoupling))).

Linked data has the downside here that data relationships may need to be
resolved at the time of access granting (((access granting))), thus effectively
re-introducing parts of an authorization query (((authorization query))) again
at this point.

The concept of distributed systems underlying linked data thus differs
fundamentally from the one in {{RM3420}}. Where the former treats distribution
as distribution of concerns across different services providing parts of the
linked data set, the latter is more concerned with resilience, specifically how
to continue operating in the (temporary) absence of such services.

## SPKI Certificate Theory {#sec:prior-spki-certs}

Cryptographic capabilities are not a new concept. {{?RFC2693}} provides
a thorough analysis of how they may be modelled in {{?X.509}} certificates,
and includes a discussion on various methods for modelling authorization
that also draws comparison to PGP above.

The two major drawbacks of this document are:

1. Being a thorough theoretical discussion, it is dense. It does not provide
   an easy path for implementors to design capability based authorization
   systems; as a reference, however, it is indispensable.
2. With its focus on X.509 certificates as the encoding and transport mechanism
   for authorization material, it does not lend itself well to a number of use
   cases; this may be why overlapping proposals exist.

   Recall how DTLS {{?RFC9147}} contains provisions for handling large
   certificates, and that JWT and ZCAP-LD provide serializations more suitable
   to web technology. Arguably, a discussion of the principles of authorization
   removed from the specifics of X.509 are required.

In the light of that RFC, this document focuses on relative simplicity and
freedom from specific serialization formats for capabilities.

## Macaroons {#sec:prior-macaroons}

Capabilities bear resemblance to Macaroons, presented in 2014 {{MACAROONS}}.
The Macaroons paper outlines a system of *caveats* under which access may be
granted to the bearer of a macaroon token. Macaroons are secured using an HMAC.
The paper also lays out the potential use of public-key based security for
Macaroons.

Macaroons may be viewed as a specific implementation of the concept of
capabilities with the focus of providing authorization in cloud environments.
The paper positions the solution primarily as adding finer granularity to bearer
tokens. In doing so it lays out a more complex caveat scheme than this document,
which excludes the specifics of the privileges that may be issued.

## CapBAC {#sec:prior-capbac}

The CapBAC system {{CAPBAC}} is focused on IoT applications, and in particular
attempts to solve the complexity problems in this application domain. The
paper is predominantly concerned with mapping abstract authorization concepts
into specific privileges encoded in a capability. It is thus an excellent
companion and motivation for this document.

The focus of this document, by contrast, is on mapping out the minimal necessary
components of making capabilities workable.

## Power of Attorney {#sec:poa}

The oldest kind of prior work in this field is the concept of Power of Attorney,
as exercised throughout much of human history.

In a Power of Attorney system, an authority (a king, etc.) grants a token
(an official seal, ...) to a subordinate which makes this subordinate
recognizable as carrying some of the king's powers and privileges.

Modern day Power of Attorney systems abound, and may be formalized as notarized
letters granting such and such rights to other people.

Capability-based authorization schemes are no different to this kind of system
in principle. In both kinds of systems, the token itself encodes the privileges
of the bearer. {{POA-IOT}} describes such a system for the Internet-of-Things.

# Elements of a Distributed Authorization Scheme {#sec:elements}

As explored in the previous sections, the most fundamental aspect of a
distributed authorization scheme is that it decouples access granting
(((access granting))) from authorization queries (((authorization query))) by
serializing the results in such a way that they can be transmitted and
evaluated at a later date. This effectively shifts the focus of distributed
authorization systems away from request tuples (((request tuple))) towards
authorization tuples. (((authorization tuple)))

More specifically, it moves the authorization query step from the real-time
phase to the preparation phase, much as public key based authentication does.

~~~ aasvg
    I. Preparation             II. Real-Time

  +--------------------------+
  | Authorization Management |
  +--------------------------+

  +---------------------+
  | Authorization Query |
  +---------------------+

           --- arbitrarily long delay ---

                              +-----------------+
                              | Access Granting |
                              +-----------------+

~~~
{:
  artwork-svg-options="--spaces=2"
  title="Phases in Capability-Based Authorization"
}

It similarly employs public key cryptography to achieve this. Once the
authorization query has resulted in the presence or absence of an 
*authorization tuple*, that fact is recorded in some serialized and signed
data, the *capability* (((!capability))) token itself. This can then be
verified later for access granting.

This implies certain things about the contents of a capability token, but
it also introduces other elements and roles into the overall scheme.

## Grantor {#sec:elements-grantor}

A grantor, sometimes called principal, has authority over an object,
(((object))) and generates authorization tuples (((authorization tuple)))
for use in the overall system.

As we describe cryptographic systems, a grantor is represented by an asymmetric
key pair. Endowment for a grantor is out of scope of this document; for
the purposes of distributed authorization, the grantor key pair *is* the grantor.

### Grantor Identifier {#sec:elements-grantor-id}

A grantor identifier uniquely identifiers the public key of the key pair; this
may be identical to a serialized form of the public key itself, or a
cryptographic hash over it (fingerprint), or some alternative scheme.

What is sufficient is that there MUST exist a mechanism for uniquely mapping the
grantor public key to the grantor identifier and vice versa. This mapping
permits verification.

### Grantor Signature {#sec:elements-grantor-sig}

The grantor undersigns a capability by adding a cryptographic signature to it.

### Grantor vs. Issuer {#sec:elements-grantor-issuer}

It's worth noting that this makes the grantor identical to the more commonly
used term of an "issuer". That latter term is useful when discussing
cryptographic actions, i.e. the issuance of a signature and by extension the
signed data.

However, beyond cryptography, the term has little semantic meaning. It does not
describe the meaning behind issuing a signature. The grantor term by contrast
encodes that in issuing a cryptographic signature, a privilege is granted. It
has meaning for the domain of authorization.

## Agent {#sec:elements-agent}

The agent is the element in a distributed system that executes a requested
action after verifying a capability. It typically manages objects itself, or
provides access to them.

## Verifier {#sec:elements-verifier}

The verifier is a role in the system that verifies a capability. While verifiers
can exist in a variety of system nodes, it's a mandatory part of the agent role.

Outside of the agent, verifiers may exist in intermediary nodes that mediate
access to agents. An example here might be an authorization proxy that sits
between the public internet and a closed system. While it may not be an agent
in and of itself, it can still decide to reject invalid requests, and only
forward those to agents that pass verification and its own forwarding rules.

## Time-Delayed Transmission Channel {#sec:elements-channel}

We introduce the concept of a time-delayed transmission channel to illustrate
that communications between grantor and verifier is not possible in real-time.

This is not fundamentally different to the delay between the preparation and
real-time phase employed so far in this document, except to add the requirement
that the capability is transmitted *at some point* during this delay.

In practice, this could be at the beginning of the delay, or when the access
request is made. They key point, however, is that this transmission of the
capability encompasses *all* the communication that occurs between the functions
in the real-time phase, and those in the preparation phase, and thus decouples
these functions in time.

## Grantee {#sec:elements-grantee}

The grantee is the entity to which a privilege is granted.

A grantee SHOULD also be represented by an asymmetric key pair in order to
perform distributed authentication.

### Grantee Identifier {#sec:elements-grantee-identifier}

A grantee identifier is the identifier used as the subject (((subject))) in
an authorization tuple.

If the grantee is equivalent to an asymmetric key pair, it MUST also be possible
to map the grantee identifier to the grantee public key and vice versa. Such a
mapping SHOULD be feasible to perform without connectivity in order to maintain
the distributed authentication mechanisms achieved through the use of asymmetric
cryptography.

## Object {#sec:elements-object}

An object is a resource the grantee wishes to access. This can be a file, or
a networked service, etc.

### Object Identifier {#sec:elements-object-identifier}

The object identifier uniquely identifiers an object. This document places no
syntactic restrictions upon the object identifier, other than that there exists
a canonical encoding for it. For the purposes of cryptographic signing and
verification, the object identifier MUST be treated as equivalent to its
canonical encoding.

## Privilege {#sec:elements-privilege}

A privilege encodes whether an action (on an object) is permitted (for a
subject); see {#sec:authorization} for an explanation.

For the purposes of creating capabilities, a privilege must have a canonical
encoding. The semantics of each privilege are out of scope of this document,
and to be defined by the systems using distributed authorization.

That being said, a typical set of privileges might include read and write
privileges for file-like resources.

## Validity Metadata {#sec:elements-validity-metadata}

In practical applications of distributed authorization scheme, validity of a
capability may be further scoped. We already discussed the need to scope it
to an authorization tuple, but further restrictions are likely desirable.

For example, a set of `not-before` and `not-after` timestamps exist in e.g.
[X.509] certificates; similar temporal validity restrictions are likely
required in practical systems.

However necessary they may be in practice, however, such additional validity
metadata has no bearing on the fundamental concepts outlined in this document,
and is therefore considered out of scope here.

## Capability {#sec:elements-capability}

A capability (((!capability))) provides a serialized encoding of previously
listed elements:

1. Fundamentally, a capability MUST encode an authorization tuple, consisting
   of:
   1. A subject (((subject))) identifier.
   1. A privilege. (((privilege)))
   1. An object (((object))) identifier.
1. A grantor identifier MAY be required in order to identify the grantor key
   pair used in signing and verification.
1. Validity Metadata SHOULD be included in practical systems.
1. In order for a verifier to ensure the validity of a capability, it MUST
   finally contain a grantor signature over all preceding fields.

The authorization tuple permits an agent to determine what kind of access to
grant or deny. The grantor identifier provides information to the verifier
about key pairs used in the authorization. While the signature proves to the
verifier that the grantor did indeed authorize access, the validity metadata
limits access to whichever additional scope the grantor decided upon.

### Extensions {#sec:elements-capability-extensions}

Note that each of the fields in an authorization tuple may be treated as a list
of zero or more such elements. While a longer discussion of this is out of scope
for this document, two notes should be made:

1. Implementations must provide clarity what it means to provide a list. Does
   the capability apply to each element in the list individually, or to some
   combination? This is highly specific to the semantics of each capability,
   so cannot be covered here.
1. A tuple consisting of a subject and privilege only (zero objects) effectively
   turns into a statement about the subject, and no longer relates to
   authorization concerns. However, other aspects of a distributed trust system
   still apply. This is the approach taken by Pretty Good Privacy.

## Authorization Process {#sec:elements-process}

Having identified the elements, we can now describe an abstract process
in a distributed authorization system.

~~~ aasvg
                  I. Preparation
   .----------------------------------------------------.
  |                                                     |
  | +---------+  2. serializes & signs    .----------.  |
  | | Grantor +------------------------->| capability | |
  | +---------+                           '----------'  |
  |     ^                                               |
  |     | 1. *authorization query* & response           |
  |     v                                               |
  |  .-------------.                                    |
  | |               |                                   |
  | |'-------------'|                                   |
  | | authorization |                                   |
  | |  tuple store  |                                   |
  |  '-------------'                                    |
  '-------------+--------------------------------------'
                ║
                ║ time-delayed transmission channel
                ║    .----------.
                ║   | capability |
                ║    '----------'
                v
   .----------------------------------------------------.
  |                                                     |
  | +---------+  1. access request   +-------+          |
  | |         +--------------------->|       |          |
  | | Grantee |                      |       |          |
  | |         |<---------------------+ Agent +---+      |
  | +---------+  4. *access grant*   |       |   |      |
  |                                  |       |   |      |
  |                       +--------->|       |   |      |
  |                       |          +-------+   |      |
  |                       |                      |      |
  |       3. verification |      2. verification |      |
  |            response   |           request    |      |
  |                       |                      v      |
  |                       |                +----------+ |
  |                       +----------------+ Verifier | |
  |                                        +----------+ |
  '-----------------------------------------------------'
                  II. Real-Time
~~~
{:
  artwork-svg-options="--spaces=2"
  title="Process for Capability-based Distributed Authorization"
}

The process is split into the two preparation and real-time phases.

In the first phase, the grantor issues an authorization query (((authorization
query))) to an authorization tuple store, which stands in here for the specific
process by which authorization is managed, and produces tuples. Based on the
response, it serializes a capability and adds its signature over it.

The capability then gets transmitted via the time-delayed transmission channel
to the second phase, providing temporal decoupling between the phases.

In the second phase, the grantee requests access to some object from the
agent. The agent must send a verification request to the verifier (which may
be a subroutine of the agent; no network transmission is implied here). The
verifier responds by either permitting access or not. If access is permitted,
the agent grants access to the grantee. Because the capability encodes all
required information for the verifier to perform this step, it does not need
access to the authorization tuple store itself.

Note that the capability can be transmitted to any entity in the second phase;
all that is relevant is that it ends up at the verifier. If it is transmitted
to the grantee, it has to pass it on to the agent as part of the access request.
If the agent receives it, it has to pass it on to the verifier as part of the
verification request.

# Delegation of Authority {#sec:delegation}

One of the more powerful applications of the power of attorney system is that
it is possible to further delegate authority. The constraint is that no entity
can provide more authority in a sub-grant than it possessed in the first place.

The ability to generate sub-grants is easily provided in a specialized privilege.
Such a privilege must encode the specific other privileges a grantee may in turn
grant to other parties.

As this may include the ability to grant further sub-grants, implementations
MUST take care here. They MAY wish to include a limit on the depth to which
sub-grants may be further delegated.

# Related Considerations {#sec:considerations}

## Human Rights Considerations {#sec:human-rights-considerations}

This document lists human rights considerations as a use case, see {{sec:uc-hrpc}}.

## Protocol Considerations {#sec:protocol-considerations}

There are no specific protocol considerations for this document.

However, protocols transmitting capabilities MAY provide some relief to
human rights concerns {{sec:uc-hrpc}}, e.g. by providing confidentiality via
encrypted transmission.

## Security Considerations {#sec:security-considerations}

This document does not specify a network protocol. In fact, it deliberately
requires no specific protocol for transmitting capabilities. As such, much of
{{BCP72}} does not apply.

However, distributed authorization does not require the invention of new
cryptographic constructs; the document is deliberately phrased such that the
choice of such constructs remains implementation defined.

As such, some security considerations are supported by the use of capabilities
for distributed authorization, such as preventing unauthorized usage and
inappropriate use.

Some notes on specific considerations follow.

### Denial of Service {#sec:security-considerations-dos}

Denial of service mitigation is out of scope, because this document does not
describe a protocol.

However, as avoiding a single point of failure ({{sec:authorization-spof}})
is one of the problems that distributed authorization schemes address, it can
easily be argued that preventing denial of service is a major concern of this
document, and consequently fully addressed here.

### Revocation {#sec:security-considerations-revocation}

As ICAP was criticized for introducing a centralized solution for revocatins,
(see {{sec:prior-icap}}), a modern distributed authorization system must
adequately consider these.

Fortunately, anything that can encode the granting of a privilege can also
encode the removal of said grant, by - essentially - encoding a negative
privilege. Doing so provides distributed revocations by the same overall
mechanism that distributed authorization is provided. A sequence of grants
and revocations for a particular request tuple will map to a sequence
of Boolean values, and can so be understood as a bit stream.

This introduces a new requirement, namely that verifiers can reconstruct
the bit stream in order to understand the latest, most up-to-date state.
Unfortunately, this can be hard due to the time-delayed nature of the
transmission channel.

Fortunately, research into conflict-free replicated data types has yielded
several methods for ordering also partially received streams, which can be
applied here by providing appropriate validity metadata. This yields eventually
consistent states in a distributed authorization system, which in many cases
may be sufficient.

It is not the purpose of this document to prescribe any particular method
for ordering grants and revocations into a consistent stream, nor whether
revocations are used at all. However, implemtations MUST take care to consider
this aspect.

### Replay Attacks {#sec:security-considerations-replay-attacks}

As presented, capabilities can be arbitrarily replayed, which suggests that
replay attacks are possible.

This is only somewhat correct. It is correct that an attacker *who has
successfully authenticated with the key specified in the capability* can
successfully replay capabilities at will.

The difference to traditionally authorizing systems is that changes in
the authorization tuple store do not reach the second phase until revocation
tokens have been issued and distributed. The risk here is exactly the same
as in e.g. TLS based systems without real-time certificate revocation lists
{{?RFC5280}}.

In principle, nothing prevents a system employing capabilities to add similar
real-time revocation lists. It must be noted that such a system will then
re-introduce a potential single point of failure.

Depending on the security guarantees the system intends to provide, a better
approach, and generally the approach envisioned here, may be to scope the
validity of capabilities to a suffiicently short time frame that such
replays are unlikely to cause significant harm.

In deciding how to guarantee appropriate levels of security, it should also
be considered that it is in the nature of fully distributed systems that
information may always be inconsistently distributed -- therein lies their
greatest risk. At the same time, this risk of inconsistency is also what
produces their greatest appeal, namely resilience in the face of partial
communication or system failures.

## Privacy Considerations {#sec:privacy-considerations}

As part of supporting human rights considerations as a first class use case,
exploring privacy considerations as covered by {{!RFC6973}} is worthwhile.

In particular, distributed authorization schemes address the concerns of:
intrusion and misattribution (as related to pseudonyms only).

It's also worth highlighting that surveillance and stored data concerns,
as well as disclosure, are *not* addressed. In order for distributed
capabilities to work, *any* likely recipient needs to be able to decode them.

The threat model then assumes that all capability data is accessible to
anyone, which is why the use of pseudonymous public-key based identifiers
is suggested. Sufficient care must be taken in key rotation, etc. in order
to provide additional protections.

Note that despite this, nothing prevents a system from encrypting capabilities
for use only by a single authorized party, which means that these last
concerns can be addressed in the surrounding system.

## IANA Considerations {#sec:IANA}

This document has no IANA actions.

--- back

# Acknowledgments {#sec:ack}
{:numbered="false"}

Jens Finkhäuser's authorship of this document was performed as part of work
undertaken under a grant agreement with the Internet Society Foundation
{{ISOC-FOUNDATION}}.
