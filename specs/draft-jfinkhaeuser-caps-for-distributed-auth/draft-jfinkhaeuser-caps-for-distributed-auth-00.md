---
###
# Internet-Draft Markdown Template
#
# Rename this file from draft-todo-yourname-protocol.md to get started.
# Draft name format is "draft-<yourname>-<workgroup>-<name>.md".
#
# For initial setup, you only need to edit the first block of fields.
# Only "title" needs to be changed; delete "abbrev" if your title is short.
# Any other content can be edited, but be careful not to introduce errors.
# Some fields will be set automatically during setup if they are unchanged.
#
# Don't include "-00" or "-latest" in the filename.
# Labels in the form draft-<yourname>-<workgroup>-<name>-latest are used by
# the tools to refer to the current version; see "docname" for example.
#
# This template uses kramdown-rfc: https://github.com/cabo/kramdown-rfc
# You can replace the entire file if you prefer a different format.
# Change the file extension to match the format (.xml for XML, etc...)
#
###

coding: utf-8
title: "Capabilities for Distributed Authorization"
abbrev: "Capabilities for Distributed Authorization"
docname: draft-jfinkhaeuser-caps-for-distributed-auth-00
category: info
submissiontype: independent
ipr: interpeer
date: 2022-12-05
stand_alone: yes
pi:
- toc
- sortrefs
- symrefs
- docmapping
- comments # TODO remove comments

number:
v: 3
# TODO
# consensus: true XXX not for 'independent'
# area: AREA
workgroup: Interpeer Project
keyword:
 - capabilities
 - authorization
 - authentication
 - power of attorney
 - cryptography

author:
 -
  ins: J. Finkhaeuser
  fullname: Jens Finkhäuser
  asciiFullname: Jens Finkhaeuser
  org: Interpeer gUG (haftungsbeschraenkt)
  abbrev: Interpeer
  # TODO add mailing address
  email: ietf@interpeer.io
  uri: https://interpeer.io/
 -
  ins: S. D. Penna
  fullname: Sérgio Duarte Penna
  asciiFullname: Sergio Duarte Penna
  org: Instituto Superior de Engenharia do Porto
  abbrev: ISEP
  street: Rua Dr. António Bernardino de Almeida, 431
  code: 4249-015
  city: Porto
  country: Portugal
  email: sdp@isep.ipp.pt
  uri: https://isep.ipp.pt/

normative:
  NIST.IR.8366: DOI.10.6028/NIST.IR.8366
  RM3420: DOI.10.7249/RM3420
  BCP72: RFC3552

informative:
  I-D.draft-knodel-terminology-10:
  I-D.draft-irtf-hrpc-guidelines-16:
  ISOC-FOUNDATION:
    title: "Internet Society Foundation"
    author:
      org: "Internet Society Foundation"
    target: https://www.isocfoundation.org/
  X.509: ITU.X509.2000
  OCAP: DOI.10.1145/365230.365252
  ICAP: DOI.10.1109/SECPRI.1989.36277
  ADACORSA:
    title: Airborne data collection on resilient system architectures
    date: 2020-05-01
    target: https://www.kdt-ju.europa.eu/projects/adacorsa
  RDF:
    title: "RDF 1.1 Concepts and Abstract Syntax"
    date: 2014-02-25
    author:
      org: RDF Working Group of the World Wide Web Consortium (W3C)
    target: https://www.w3.org/TR/rdf11-concepts/


--- abstract

Authorization is often the last remaining centralized function in a distributed
system. Advances in compute capabilities of miniaturized CPUs make alternative
cryptographic approaches feasible that did not find such use when first
envisioned. This document describes the elements of such cryptographically backed
distributed authorization schemes as a reference for implementations.

--- middle

[^fixme]: FIXME

# Introduction {#sec:intro}

In 1964, Paul Baran at the RAND Corporation described centralized, decentralized
and distributed communications networks and their properties {{RM3420}}. Baran's
argument was that because in distributed systems, each node can reach many other
nodes, failure of a single node need not impact the ability of other nodes to
communicate.

This resilience is desirable in distributed systems today. Therefore it seems
an oversight that authentication and authorization in modern system is often a
centralized function.

This document explores previous attempts at distributed authorization schemes,
and outlines common elements of such solutions in order to provide a reference
for future work.

## Conventions and Definitions {#sec:intro-conventions}

{::boilerplate bcp14-tagged}

In order to respect inclusive language guidelines from {{NIST.IR.8366}} and
{{?I-D.draft-knodel-terminology-10}}, this document uses plural pronouns.

## Problem Space {#sec:problem}

Distributed authorization is not a goal in itself, but may be desirable in
distributed systems.

It's also worth exploring how the distribution of authorization functions
related to authentication. In many systems, these are intrinsically linked.
Logging in with a user name and password is one such example. Providing the
correct password proves that the person at the keyboard is authorized to access
a resource. But at the same time, providing the correct password in combination
with a user name authenticates this user. Furthermore, any permissions granted
to the user are typically linked to the user name, as that remains stable
throughout password changes.

### Authentication {#sec:authentication}

Password-based authentication mechanisms require that the tuple of user name and
password (or password hash) are sent to some central repository where records of
such tuples are kept; if the tuple is found, the user name is authenticated.

This common scheme mixes different aspects to authentication, however, which are
worth disambiguating.

Endowment:
: (((!endowment))) The act of logging in establishes an association between a
user name and the person interacting with the device. More broadly speaking,
(parts of) a three-way endowment are performed: an *identifier* is endowed
with *attributes*, which describe a *person* in sufficient detail to identify
them.

Secret Proving:
: (((!secret proving))) Logging in also proves that the person interacting with
the device is in possession of some secret, which should only be known to the
person which merits having the user name linked to the secret associated with
them.

The distinction becomes somewhat more relevant when we move towards distributed
authentication schemes, which rely on public key cryptography.

#### Web of Trust {#sec:authentication-wot}

In Web of Trust based systems, starting with Philip R. Zimmermann's Pretty Good
Privacy (PGP), public keys are exchanged with some metadata attached. This
performs some part of endowment (((endowment))) in that it provides
the link between a public key and a user identifier (see
{{?RFC4880, Section 11.1}}).

Other parts of endowment are not specified. These often consist of manual
checks that the user identifier belongs to some person holding the corresponding
private key, and may involve verifying of government issued identification
documents. Once such a check is passed, the verifier issues a digital signature
over the tuple of user identifier and public key to provide some proof that the
verification has occurred.

Endowment in Web of Trust occurs when a sufficient number of sufficiently
trustworthy signatures have been reached. Which number of trust level is deemed
sufficient is in the control of the recipient of a transferable public key
packets, however.

#### TLS Certificates {#sec:authentication-tls}

A similar concept is applied in TLS {{?RFC8446}}, where {{?X.509}} certificates
are used for endowment.

The major difference to Web of Trust based systems is how trust is established.
Instead of relying on a recipient defined method of determining trust,
certificates are issued by one of a set of well-known trust sources. Information
on these is stored in root certificates, which are distributed to the machines
participating in the system.

While there are globally issued root certificates for entities that perform
endowment professionally, it is always possible for a system designer to
include other root certificates.

#### Secret Proving {#sec:authentication-secret}

Neither {{?X.509}} certificates nor the transferable public key packets in
{{?RFC4880}} provide any means for secret proving. This is left to other parts
of TLS or PGP.

In TLS, the handshake during connection establishment is used to send challenges
that only machines with the correct private key can respond to. PGP, which aims
to provide privacy at rest, simply encrypts content with a secret key which is
then encrypted with the recipient's public key. Other parties cannot decrypt this,
which keeps content safe.

TLS and PGP are not the only public key cryptography based authentication systems,
but they can stand in for the two most common classes of such systems: one aims
to establish trust from authoritative sources. The other aims to establish trust
based on the trust requirements of the recipient.

Both systems also strictly speaking separate endowment from secret proving.
While in TLS the certificates are transmitted as part of the overall handshake,
creating certificates nevertheless occurs beforehand. This temporal decoupling
(((!temporal decoupling))) is a key property that may also be applied to
authorization.

### Authorization {#sec:authorization}

Authorization occurs only after secret proving. Once an identity has been
established, it is then mapped to privileges associated with said entity,
which determine which object(s) it has access to.

There exist a plethora of methods to establish this mapping. Access-control
lists (ACL) simply provide tuples of identities, privileges and associated
objects. Role-based access control (RBAC) is effectively identical, if the
identities specified are not those of individuals, but of groups (as a group
member, an individual inhabits the associated role). A comparable approach is
Organization-based access control (OrBAC), which not only abstracts the
identity to that of a role, but performs a similar abstraction on the object
and privilege.

More complex systems such as context- or lattice-based access control (CBAC and
LBAC respectively) derive a mapping from properties of or labels attached to the
individuals and objects. Finally, graph-based access control (GBAC) starts with
a graph of an organization, and derives privileges from the properties inherited
by being part of a larger organizational structure.

What these systems address is the problem of *managing* the mapping of an
identity to access privileges for objects, where each system has advantages and
disadvantages for various use cases.

Subject:
: The subject (((!subject))) is the identity (individual or group/role) that
intends to perform an action.

Action:
: The action (((!action))) the subject intends to perform may be as simple as
reading or writing a resource, but can be more complex.

Object:
: Actions are performed on objects (((!object))), such as a file or network
resource.

Request Tuple:
: A request tuple (((!request tuple))) consists of the subject, action and
(optional) object.

Privilege:
: A privilege (((!privilege))) encodes whether or not an action is permitted.

Authorization Tuple:
: An authorization tuple (((!authorization tuple))) encodes system state, and
is thus a tuple of a subject, a privilege and an (optional) object.

The act of authorization translates from a request tuple to a Boolean response
determining whether a request is permitted. A centralized authorization function
provides this answer in real-time. Distributed authorization instead deals in
authorization tuples.

It may be of interest that and authorization tuple is semantically equivalent
to an RDF triple ([RDF]), in that it encodes a specific relationship between a
subject and an object. Authorization tuples that consists solely of IRIs
{{?RFC3987}} is also syntactically an RDF triple. This implies that
authorization tuples can encode arbitrarily complex authorization information by
building the knowledge graph resulting from resolving such an RDF triple.

#### Single Point of Failure {#sec:authorization-spof}

A centralized function is very useful for managing authorization. The previous
section on different access control methods should illustrate sufficiently that
authorization management is a complex problem; complex enough for multiple
competing management methods to emerge.

Faced with such a complex problem, it is no surprise that solutions tend to
bring this function to a centralized location. Managing this complexity in one
place is of course simpler than managing it across multiple locations.

The downside to this is that failure of this single location may mean failure
of the system as a whole. Particularly vulnerable to this single point of failure
are systems in which all access is controlled by specific privileges. Systems
with publicly available parts may still provide those functions that do not rely
on any privileges.

#### Temporal Coupling {#sec:authorization-temp}

The other class of problems with centralized authorization relate to the
temporal coupling of granting access (((!access granting))) and resolving
authorization queries (((!authorization query))). The abstract request
introduced above of resolving an request tuple (((request tuple))) to a
Boolean response tightly couples both steps.

This requires disambiguating between participants in such a system somewhat.
From the perspective of the person operating the access control management
system, granting access occurs whenever they make an entry into the database.
The machine permitting an authorized user to perform an action, however, grants
or denies access in the moment the action is requested. If this second form
of access granting is based on a real-time authorization query, it couples
granting access to such a query in time.

The key insight into distributing authorization effectively is that it has
little to do with managing access control databases. Rather, it is related to
temporally decoupling the authorization query from granting access.

## Previous Work {#sec:prior}

Dividing the authentication problem into endowment and secret proving helps
illustrate how web of trust systems introduce temporal decoupling between these
functions, in a way that e.g. TLS does not.

In much the same way, dividing the authorization problem into querying an
authorization database and granting access to an object suggests that
authorization, too, can be temporally decoupled.

This section lists prior work where some temporal decoupling of this kind has
been performed in the past.

### Object-Capabilities (OCAP) {#sec:prior-ocap}

Dennis and Van Horn described an approach for securing computations in
"multiprogrammed" systems in 1965/66 ({{OCAP}}). The context in which they operated had
little to do with modern distributed systems.

However, they recognized the trend of running computing systems complex enough
that multiple programmers would contribute to its overall function. This raised
a desire for access control to individual sub-functions, which a security kernel
within the operating system was to provide.

The key differentiator to other systems was that in OCAP, a calling process was
to present a "capability", a serialized token to the process being invoked. This
capability was intended to encode all relevant information the called process
would need to determine whether the caller was permitted to perform such an
action.

These properties of being serializable and containing all relevant authorization
information imply that, conceptually, capabilities are cached results of an
authorization query (((authorization query))). The called process can then perform
access granting (((access granting))) without issuing such a query itself, thereby
temporally decoupling the two functions.

### Identity-Capabilities (ICAP) {#sec:prior-icap}

The OCAP system proved to have a particular weakness, namely that "the right to
exercise access carries with it the right to grant access". This is the result
the information encoded in an OCAP capability: it contains a reference to the
object and action to perform, but does not tie this to any identity.

In 1988, Li Gong sought to address this with an Identity-Capability model
({{ICAP}}). Including an identity in the capability token arrives at the
authorization tuple (((authorization tuple))) in {{sec:authorization}}. Furthermore,
ICAP introduces the notion of capability use in networked systems. ICAP does this by
temporally decoupling the authorization query from access granting.

The main criticism levelled against the paper and capability-based approaches in
general in the following years was that some functions were missing, such as a
check for revocations. Proposals to address this often added centralized
functions again, which led to criticism of the distributed approach in general.

### Pretty Good Privacy {#sec:pgp}

While we previously discussed PGP in terms of authentication in
{{sec:authentication-wot}}, a key property of PGP is the introduction of trust
signatures ({{?RFC4880, Section 5.2.3.13}}).

Trust signatures do not merely authenticate a user, they introduce a kind of
authorization as well, as they carry specific notions for what the provided
public key may be trusted for. The trust signature thus encodes specific kinds
of privileges (((privilege))) of an authorization tuple (((authorization tuple))),
while the public key encodes a subject (((subject))). The only component
missing in the tuple is the object (((object))).

While the authorization tuple in PGP is incomplete, the system is based on
public key cryptography, and can thus be used to securely verify a binding
between the tuple elements.

### JSON Web Tokens (JWT) {#sec:jwt}

JSON Web Tokens ({{?RFC7519}}) provide a standardized way for serializing
access tokens. Current uses are in systems with centralized authorization
functions such as OAuth ({{?RFC6749}}).

However, the fundamental notion of capabilities, that a serializable token
carries authorization information, is provided also here. Furthermore, JWT
combines this with cryptographic signatures, providing for - in theory -
temporal decoupling as previously discussed.

It's well worth pointing out that JWT is suitable as a portable, modern
capability format - all it requires is to encode all necessary information
within its fields.

### Power of Attorney {#sec:poa}

The oldest kind of prior work in this field is the concept of Power of Attorney,
as exercised throughout much of human history.

In a Power of Attorney system, an authority (a king, etc.) grants a token
(an official seal, ...) to a subordinate which makes this subordinate
recognizable as carrying some of the king's powers and privileges.

Modern day Power of Attorney systems abound, and may be formalized as notarized
letters granting such and such rights to other people.

Capability-based authorization schemes are no different to this kind of system
in principle. In both kinds of systems, the token itself encodes the privileges
of the bearer.

## Use Cases {#sec:use-cases}

Use cases relate to one or more of the issues explored in the problem space.

### IoT On-boarding {#sec:uc-iot}

On-boarding IoT devices into an overall system requires authentication and
authorization; this may be mutual.

In such scenarios, new devices rarely have connectivity before completing
on-boarding. It follows that authentication and authorization must work in
a fully offline fashion, which in turn requires that authorization tokens
provided to the device contain all information required for the authorization
step. As described in {{sec:prior-ocap}}, this translates to a requirement
of temporally decoupling access granting from an authorization query.

### UAV Control Handover {#sec:uc-uav}

A similar argument applies to control handover of unmanned aerial vehicles
(UAV). The concept of Beyond Visual Line of Sight (BVLOS) missions is to send
drones into places that are harder or more costly to reach for humans.

Control handover refers to transferring operational control for a drone from one
ground control station to (GCS) another. Control handover bears similarities to IoT
on-boarding in that the drone is on-boarded to a new control system (and the
previous system relinquishes control).

In general, aviation authorities such as FAA, EASA, etc. expect control handover
to occur under ideal circumstances, in which centralized authorization schemes
suffice. There is, however, a class of scenarios where connectivity to a central
service cannot be guaranteed.

#### Remote Location {#sec:uc-uav-remote}

In order to guarantee BVLOS operations in very remote locations, research
projects such as {{ADACORSA}} assume use cases in which two ground control
stations between which handover occurs to not have connectivity to each other.

In such cases, it is necessary for the UAV to act as a time-delayed transmission
channel for authorization information between the GCSes.

#### Emergency Response {#sec:uc-uav-emergency}

Emergency response teams may require UAVs in the vicinity to immediately clear
the airspace and go to ground. This effectively translates to the emergency
response team operating a ground control station that takes over control and
issues a single command.

As emergency responses are, by definition, typically required in situations
where normal operations cannot be assumed, this includes the assumption that
connectivity cannot be assumed. Nevertheless, such an emergency control
handover must be possible.

#### Mobile Ground Control Stations {#sec:uc-uav-mobile-gcs}

A comparable scenario to the above describes situations in which UAV attach
to a mobile ground control station. Specific scenarios may range from cave
exploration to investigating burning buildings.

The commonality here is that the UAV cannot establish connectivity to a wider
system, but can connect to the mobile GCS. This in turn may act as a
communications relay to the outside world, but may be too limited in capacity
to permit online, centralized authorization.

# Elements of a Distributed Authorization Scheme {#sec:elements}

As explored in the previous sections, the most fundamental aspect of a
distributed authorization scheme is that it decouples access granting
(((access granting))) from authorization queries (((authorization query))) by
serializing the results in such a way that they can be transmitted and
evaluated at a later date. This effectively shifts the focus of distributed
authorization systems away from request tuples (((request tuple))) towards
authorization tuples. (((authorization tuple)))

This implies certain things about the contents of a capability token, but
it also introduces other elements and roles into the overall scheme.

## Grantor {#sec:elements-grantor}

A grantor, sometimes called principal, has authority over an object,
(((object))) and generates authorization tuples (((authorization tuple)))
for use in the overall system.

As we describe cryptographic systems, a grantor is represented by an asymmetric
key pair. Endowment for a grantor is out of scope of this document; for
the purposes of distributed authorization, the grantor key pair *is* the grantor.

### Grantor Identifier {#sec:elements-grantor-id}

A grantor identifier uniquely identifiers the public key of the key pair; this
may be identical to a serialized form of the public key itself, or a
cryptographic hash over it (fingerprint), or some alternative scheme.

What is sufficient is that there MUST exit a mechanism for uniquely mapping the
grantor public key to the grantor identifier and vice versa. This mapping
permits verification.

### Grantor Signature {#sec:elements-grantor-sig}

The grantor undersigns a capability by adding a cryptographic signature to it.

## Agent {#sec:elements-agent}

The agent is the element in a distributed system that executes a requested
action after verifying a capability. It typically manages objects itself, or
provides access to them.

## Verifier {#sec:elements-verifier}

The verifier is a role in the system that verifies a capability. While verifiers
can exist in a variety of system nodes, it's a mandatory part of the agent role.

Outside of the agent, verifiers may exist in intermediary nodes that mediate
access to agents. An example here might be an authorization proxy that sits
between the public internet and a closed system. While it may not be an agent
in and of itself, it can still decide to reject invalid requests, and only
forward those to agents that pass verification and its own forwarding rules.

## Time-Delayed Transmission Channel {#sec:elements-channel}

We introduce the concept of a time-delayed transmission channel to illustrate
that communications between grantor and verifier is not possible in real-time.

In practice, of course the communications channel does not have to be time-
delayed. But treating it as such implies that granting access must be temporally
decoupled from the authorization query.

## Grantee {#sec:elements-grantee}

The grantee is the entity to which a privilege is granted.

A grantee SHOULD also be represented by an asymmetric key pair in order to
perform distributed authentication.

### Grantee Identifier {#sec:elements-grantee-identifier}

A grantee identifier is the identifier used as the subject (((subject))) in
an authorization tuple.

If the grantee is equivalent to an asymmetric key pair, it MUST also be possible
to map the grantee identifier to the grantee public key and vice versa. Such a
mapping SHOULD be feasible to perform without connectivity in order to maintain
the distributed authentication mechanisms achieved through the use of asymmetric
cryptography.

## Object {#sec:elements-object}

An object is a resource the grantee wishes to access. This can be a file, or
a networked service, etc.

### Object Identifier {#sec:elements-object-identifier}

The object identifier uniquely identifiers an object. This document places no
syntactic restrictions upon the object identifier, other than that there exists
a canonical encoding for it. For the purposes of cryptographic signing and
verification, the object identifier MUST be treated as equivalent to its
canonical encoding.

## Privilege {#sec:elements-privilege}

A privilege encodes whether an action (on an object) is permitted (for a
subject); see {#sec:authorization} for an explanation.

For the purposes of creating capabilities, a privilege must have a canonical
encoding. The semantics of each privilege are out of scope of this document,
and to be defined by the systems using distributed authorization.

That being said, a typical set of privileges might include read and write
privileges for file-like resources.

## Validity Metadata {#sec:elements-validity-metadata}

In practical applications of distributed authorization scheme, validity of a
capability may be further scoped. We already discussed the need to scope it
to an authorization tuple, but further restrictions are likely desirable.

For example, a set of `not-before` and `not-after` timestamps exist in e.g.
[X.509] certificates; similar temporal validity restrictions are likely
required in practical systems.

However necessary they may be in practice, however, such additional validity
metadata has no bearing on the fundamental concepts outlined in this document,
and is therefore considered out of scope here.

## Capability {#sec:elements-capability}

A capability provides a serialized encoding of previously listed elements:

1. Fundamentally, a capability MUST encode an authorization tuple, consisting
   of:
   1. A subject (((subject))) identifier.
   1. A privilege. (((privilege)))
   1. An object (((object))) identifier.
1. A grantor identifier MAY be required in order to identify the grantor key
   pair used in signing and verification.
1. Validity Metadata SHOULD be included in practical systems.
1. In order for a verifier to ensure the validity of a capability, it MUST
   finally contain a grantor signature over all preceding fields.

The authorization tuple permits an agent to determine what kind of access to
grant or deny. The grantor identifier provides information to the verifier
about key pairs used in the authorization. While the signature proves to the
verifier that the grantor did indeed authorize access, the validity metadata
limits access to whichever additional scope the grantor decided upon.

### Extensions {#sec:elements-capability-extensions}

Note that each of the fields in an authorization tuple may be treated as a list
of zero or more such elements. While a longer discussion of this is out of scope
for this document, two notes should be made:

1. Implementations must provide clarity what it means to provide a list. Does
   the capability apply to each element in the list individually, or to some
   combination? This is highly specific to the semantics of each capability,
   so cannot be covered here.
1. A tuple consisting of a subject and privilege only (zero objects) effectively
   turns into a statement about the subject, and no longer relates to
   authorization concerns. However, other aspects of a distributed trust system
   still apply. This is the approach taken by Pretty Good Privacy.

## Authorization Process {#sec:elements-process}

Having identified the elements, we can now describe an abstract process
in a distributed authorization system.

~~~ aasvg
   .----------------------------------------------------.
  |                                                     |
  | +---------+  2. serializes & signs    .----------.  |
  | | Grantor +------------------------->| capability | |
  | +---------+                           '----------'  |
  |     ^                                               |
  |     | 1. *authorization query* & response           |
  |     v                                               |
  |  .-------------.                                    |
  | |               |                                   |
  | |'-------------'|                                   |
  | | authorization |                                   |
  | |  tuple store  |                                   |
  |  '-------------'                                    |
  '-------------+--------------------------------------'
                ║
                ║ time-delayed communications channel
                ║    .----------.
                ║   | capability |
                ║    '----------'
                v
   .----------------------------------------------------.
  |                                                     |
  | +---------+  1. access request   +-------+          |
  | |         +--------------------->|       |          |
  | | Grantee |                      |       |          |
  | |         |<---------------------+ Agent +---+      |
  | +---------+  4. *access grant*   |       |   |      |
  |                                  |       |   |      |
  |                       +--------->|       |   |      |
  |                       |          +-------+   |      |
  |                       |                      |      |
  |       3. verification |      2. verification |      |
  |            response   |           request    |      |
  |                       |                      v      |
  |                       |                +----------+ |
  |                       +----------------+ Verifier | |
  |                                        +----------+ |
  '-----------------------------------------------------'
~~~
{: artwork-svg-options="--spaces=2"}

The process is split into two phases.

In the first phase, the grantor issues an authorization query (((authorization
query))) to an authorization tuple store, which stands in here for the specific
process by which authorization is managed, and produces tuples. Based on the
response, it serializes a capability and adds its signature over it.

The capability then gets transmitted via the time-delayed communications channel
to the second phase, providing temporal decoupling between the phases.

In the second phase, the grantee requests access to some object from the
agent. The agent must send a verification request to the verifier (which may
be a subroutine of the agent; no network transmission is implied here). The
verifier responds by either permitting access or not. If access is permitted,
the agent grants access to the grantee. Because the capability encodes all
required information for the verifier to perform this step, it does not need
access to the authorization tuple store itself.

Note that the capability can be transmitted to any entity in the second phase;
all that is relevant is that it ends up at the verifier. If it is transmitted
to the grantee, it has to pass it on to the agent as part of the access request.
If the agent receives it, it has to pass it on to the verifier as part of the
verification request.

# Delegation of Authority {#sec:delegation}

One of the more powerful applications of the power of attorney system is that
it is possible to further delegate authority. The constraint is that no entity
can provide more authority in a sub-grant than it possessed in the first place.

The ability to generate sub-grants is easily provided in a specialized privilege.
Such a privilege must encode the specific other privileges a grantee may in turn
grant to other parties.

As this may include the ability to grant further sub-grants, implementations
MUST take care here. They MAY wish to include a limit on the depth to which
sub-grants may be further delegated.

# Related Considerations {#sec:considerations}

## Human Rights Considerations {#sec:human-rights-considerations}

What follows is a list of objectives derived from {{!RFC8280}}, each with a
brief statement how this document addresses each concern, or why it does not.

### In Scope {#sec:hro-in-scope}

Connectivity:
: Distributed authorization observes end-to-end principle, by temporally
decoupling distinct functions in the authorization process, so that connectivity
can be almost arbitrarily unreliable.

Reliability:
: Capabilities provide reliability by removing the need for real-time
reliability.

Content agnosticism:
: The elements of authorization tuples are only defined in the abstract; as
noted in {#sec:authorization}, they can encode arbitrarily complex
relationships. Of course, the semantics SHOULD be somewhat focused on
authorization.

Integrity:
: The use of cryptographic signatures in capabilities also provides integrity
checking, but no explicit mechanism for distinguishing between integrity
failures and authenticity is provided.

Authenticity:
: Proving authenticity is a core concept of cryptographic capabilities.

Pseudonymity:
: This document deliberately distinguishes between endowment and secret
proving in {#sec:authentication}, precisely because authorization does not
need to make use of such information.

Censorship resistance:
: Temporally decoupling authorization queries from access granting provides
one element in a censorship resistant system, as it permits for capabilities
to travel out-of-band by means that circumvent potential censorship, such as
e.g. via a sneakernet, etc.

Outcome Transparency:
: This document's main focus is on illustrating the outcomes of distributed
authorization schemes.

Adaptability:
: As this document provides a generalized view on distributed authorization
schemes and capabilities, it is highly adaptable in specific implementations.

Decentralization:
: Decentralization - rather, distribution - is the goal of this document.

Open Standards:
: Care has been taken to define this specification in reference to other open
  standards (see the references section).

Security:
: As this document describes an authorization scheme, security is of great
concern. However, guidelines in {{BCP72}} do not fully apply to an abstract
description. See {#sec:security-considerations} for more detail.

### Out of Scope {#sec:hro-out-of-scope}

Confidentiality:
: Confidentiality is out of scope of the general concept of capabilities;
capability content is almost by definition public data. However, nothing
prevents an implementation from providing confidentiality for the capability
elements outside of the signature.

Heterogeneity Support:
: There is no specific heterogeneity support in distributed authorization
schemes.

Remedy:
: Remedy (new in {{?I-D.draft-irtf-hrpc-guidelines-16}} often stands in stark
  contrast to anonymity and pseudonymity, both of which are out of scope.
  Implementations must take care to balance these concerns.

Internationalization:
: This document focuses on abstract concepts; internationalization is not in
scope.

Localization
: See internationalization; this is not in scope.

Privacy:
: For privacy protections, implementations must themselves ensure that any
information encoded in a capability cannot be abused to resolve otherwise
private information.

Anonymity:
: Anonymity is out of scope for this document. While pseudonymous use is
feasible if sufficient care is taken, authorization always ties to an identity,
however transient.

Accessibility:
: Accessibility concerns are out of scope.

## Protocol Considerations {#sec:protocol-considerations}

There are no specific protocol considerations for this document.

However, protocols transmitting capabilities MAY provide some relief to
human rights concerns {#sec:human-rights-considerations}, e.g. by providing
confidentiality via encrypted transmission.

## Security Considerations {#sec:security-considerations}

This document does not specify a network protocol. In fact, it deliberately
requires no specific protocol for transmitting capabilities. As such, much of
{{BCP72}} does not apply.

However, distributed authorization does not require the invention of new
cryptographic constructs; the document is deliberately phrased such that the
choice of such constructs remains implementation defined.

### Confidentiality {#sec:security-considerations-confidentiality}

Confidentiality concerns are out of scope of this document.

### Data Integrity {#sec:security-considerations-data-integrity}

Data integrity is provided as a side effect of requiring a cryptographic
signature over capability payloads.

### Peer Entity Authentication {#sec:security-considerations-peer-entity-auth}

Peer entity authentication is not, strictly speaking, a concern of this
document. However, it should be noted that an agent in the process MUST
obviously authenticate a grantee before proceeding to verify capabilities.

As noted previously, distributed authorization does not require endowment
in this process, however.

### Non-Repudiation {#sec:security-considerations-non-repudiation}

Non-repudiation is strictly speaking out of scope of this document because
key exchange is out of scope. However, key exchange is a necessary precondition
for verifying capabilities. Non-repudiation is one of the guarantees that
verification provides.

### Unauthorized Usage {#sec:security-considerations-unauthorized-usage}

Preventing unauthorized usage is the core concern of this document.

### Inappropriate Usage {#sec:security-considerations-inappropriate-usage}

Any scheme that prevents unauthorized use may also be extended to prevent
inappropriate use. However, such additions are out of scope.

### Denial of Service {#sec:security-considerations-dos}

Denial of service mitigation is out of scope, because this document does not
describe a protocol.

However, as avoiding a single point of failure ({{sec:authorization-spof}})
is one of the problems that distributed authorization schemes address, it can
easily be argued that preventing denial of service is a major concern of this
document, and consequently fully addressed here.

### Replay Attacks {#sec:security-considerations-replay-attack}

Capabilities can be "replayed" as much as an attacker wants. As they encode a
state, such state does not change when it is received multiple times.

However, implementations MUST take care not to invite replay attacks when
designing their specific validity metadata ({{sec:elements-validity-metadata}}).
Furthermore, implementations MUST include such metadata in the signature.

### Message Insertion {#sec:security-considerations-message-insertion}

Message insertion is of no concern to this document.

### Message Deletion {#sec:security-considerations-message-deletion}

Message deletion is of little concern to this document, except insofar as if
the transmission of capabilities is disrupted, access granting cannot proceed.
Implementations MUST take care to provide safeguards against this as their
threat model requires.

### Message Modification {#sec:security-considerations-message-modification}

Message modification is prevented by the cryptographic signatures in
capabilities.

### Man-In-The-Middle {#sec:security-considerations-mitm}

As this document does not provide a protocol specification, this consideration
does not apply.

### Key Usage {#sec:security-considerations-key-usage}

This document relies on secure key exchange.

### Revocation {#sec:security-considerations-revocation}

As ICAP was criticized for introducing a centralized solution for revocatins,
(see {{sec:prior-icap}}), a modern distributed authorization system must
adequately consider these.

Fortunately, anything that can encode the granting of a privilege can also
encode the removal of said grant, by - essentially - encoding a negative
privilege. Doing so provides distributed revocations by the same overall
mechanism that distributed authorization is provided. A sequence of grants
and revocations for a particular request tuple will map to a sequence
of Boolean values, and can so be understood as a bit stream.

This introduces a new requirement, namely that verifiers can reconstruct
the bit stream in order to understand the latest, most up-to-date state.
Unfortunately, this can be hard due to the time-delayed nature of the
communications channel.

Fortunately, research into conflict-free replicated data types has yielded
several methods for ordering also partially received streams, which can be
applied here by providing appropriate validity metadata. This yields eventually
consistent states in a distributed authorization system, which in many cases
may be sufficient.

It is not the purpose of this document to prescribe any particular method
for ordering grants and revocations into a consistent stream, nor whether
revocations are used at all. However, implemtations MUST take care to consider
this aspect.

## Privacy Considerations {#sec:privacy-considerations}

This section lists privacy considerations as covered by {{!RFC6973}} and
distributed authorization's relationship to them.

### Surveillance {#sec:privacy-considerations-surveillance}

The surveillance concerns outlined in {{!RFC6973}} specifically relate to
network protocols; this document does not describe such a protocol.

As noted previously, pseudonymous use is well supported by this kind of scheme,
while fully anonymous use is not. If appropriate confidentiality is provided
by implementations, distributed authorization schemes can be considered fairly
resistant to surveillance.

That being said, systems SHOULD still use transport encryption in order to
further mitigate against surveillance.

### Stored Data Compromise {#sec:privacy-considerations-stored-data}

Stored data concerns are largely identical to surveillance concerns in the
context of distributed authorization.

### Intrusion {#sec:privacy-considerations-intrusion}

Authorization schemes in general aim to protect against intrusion.

### Misattribution {#sec:privacy-considerations-misattribution}

Misattribution in {{!RFC6973}} refers to misattribution to individuals,
which relates to endowment. This document considers endowment
out of scope.

### Correlation {#sec:privacy-considerations-correlation}

Distributed authorization tokens cannot protect against correlation, unless
confidentiality concerns are addressed. This is, however, an implementation
concern.

### Identification {#sec:privacy-considerations-identification}

Similar arguments can be made for identification concerns. Distributed
authorization is fundamentally concerned with identifying elements in a system,
but if confidentiality is provided, identification is not possible.

### Secondary Use {#sec:privacy-considerations-secondary-use}

Secondary use concerns are not in scope of this document.

### Disclosure {#sec:privacy-considerations-disclosure}

Disclosure concerns are effectively identical to confidentiality concerns in
that capabilities may leak identifiers, which may be pseudonymous.

### Exclusion {#sec:privacy-considerations-exclusion}

Exclusion is a network protocol consideration, and does not apply here.

## IANA Considerations {#sec:IANA}

This document has no IANA actions.

--- back

# Acknowledgments {#sec:ack}
{:numbered="false"}

Jens Finkhäuser's authorship of this document was performed as part of work
undertaken under a grant agreement with the Internet Society Foundation
{{ISOC-FOUNDATION}}.
