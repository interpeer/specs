# Specifications

This repository collects specs in Internet Draft markdown format.

Specifications evolve at a different pace from code. As such, a separate
repository for specifications is chosen over bundling them within code
repositories. At the same time, maintaining multiple specification repositories
seems unnecessary for the time being.

Instead, each specification is contained in its own subdirectory. The directory
name relates to the draft naming scheme for the IETF.

# Building

Building in this repository occurs in several steps, which are separated because
you may wish to perform them independently.

1. [Getting Started](#getting-started)
1. [Building Specifications](#building-specifications)
1. [Generating Content](#generating-content)
1. [Building website](#building-website)
1. [Deploying website](#deploying-website)

The overall process is to build specs as you write them, storing the outputs
in this repository as well. This way, we keep track of all changes. It also
ensures that publication timestamps in the specs are only modified when you
explicitly modify them.

When a particular set of specs should make it to the website, this needs to
be converted to Hugo content files. There is a script that does it all for
you.

You can now build the Hugo website.

Finally, when the website looks good, you can deploy it.

## Getting Started

**Common**

1. Install [python](https://python.org). Any version 3.x should work, but we're
   testing with 3.10 or later.
1. Install pipenv: `$ pip3 install pipenv`
1. Install the environment; this includes a few tools used in the building of
   the specs: `$ pipenv install`

**Specs Building**

1. Install the [kramdown-rfc](https://github.com/cabo/kramdown-rfc) ruby gem
1. Install `xml2rfc`; on [Debian](https://debian.org) and derived systems, you
   can install it via `apt install xml2rfc`

**Website Building**

1. Install [hugo](https://gohugo.io/) and [tailwindcss](https://tailwindcss.com/)
   for generating the website.

## Building Specifications

The [IETF Authors](https://authors.ietf.org) page contains details about the
markdown format and tooling for rendering documents.

Processing the input markdown files is easiest with the `kramdown-rfc`
ruby gem. This transforms the markdown into IETF compliant XML.

The `xml2rfc` binary can then be used to turn the XML file into text.

With both packages installed, you can run both processing steps with the
`kdrfc` executable.

The `specs/Makefile` is helpful in building specs. You can make specific output
files, or all files of a given type. The following assumes you're changing to
the `specs` subdirectory.

Make a specific output file:

```bash
$ make foo/foo.pdf
```

Make all files of this type:

```bash
$ make pdf
```

Note that the mechanism relies on, in this example, a symlink `foo/current.md`
pointing at `foo/foo.md` (with or without version postfix). See also the next
section.

Make all files:

```bash
$ make full
```

### Document Naming

Documents scheduled for adoption may require `group` or `source` name
components. Prior to adoption, we follow the suggested
`draft-authors-wgname-specific-subject` naming scheme, where the `wgname`
is omitted as suggested by the guidelines.

Versioning as per IETF guidelines is supported.

### Document Renaming

It is common to rename documents under IETF guidelines. With this shared
repository, we also have to rename subdirectories as a result. The convention
we're using is this:

1. Rename the directory.
1. Create a symbolic link to the renamed directory with the old name.
1. Within the directory, rename only the newest version.
1. Always have a symlink `current.md` that points to the latest document in a
   directory. This will be used to generate the website.

## Generating Content

While building the specs generates HTML files, and starts from Markdown files,
neither of these are suitable inputs for `hugo`, unfortunately. The Markdown
used in RFCs is full of unsupported extensions. At the same time, the HTML
renders a full page, with headers that are not suitable for this website.

The python script `scripts/update_content.py` collects draft files from the
`specs` directory, pre-processes them, and generates the files in the
`content` directory. This directory is in `.gitignore`, so you must generate
content before building the website.

The benefit of this approach, however, is that this keeps draft output and
website input separate, so working on the website and on the drafts are
independent activities. It also means that splitting the website repo from
the drafts repo, or using multiple independent draft repos at some future
point is more easily doable.

The script does not clear out old content files, but generates everything you
need. You can safely `rm -rf content/*` before running it.

## Building Website

Building the website uses the same general approach as in the main website
repostory, i.e. the top-level `Makefile`. In the top-level working directory,
run `make dev` to run a development server, which can be used to test rfc
specific website templates.

Note that the theme is in a separate git repository, included as a git module.

## Deploying Website

Running `make deploy` will build the site, and try to push the result to the
`pages` branch. Obviously, you'll need the right permissions to do this, so
don't.

But the `Makefile` may help illustrate how that works.
