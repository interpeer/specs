this_dir := $(abspath $(dir $(lastword $(MAKEFILE_LIST))))
theme := interpeer
theme_dir := $(this_dir)/themes/$(theme)
output_dir := $(this_dir)/output

pipenv_prefix := $(shell which pipenv) run

.PHONY: default dev prod audit audit-sitemap audit-dirs audit-files content

default: prod

### Build

content:
	@scripts/update_content.py
	@themes/interpeer/scripts/generate_cards

css:
	@$(pipenv_prefix) $(MAKE) -C $(theme_dir) css

dev: css content
	@cd $(this_dir) && \
		$(pipenv_prefix) hugo server -DEF \
			-p 1314 \
		#--noHTTPCache

prod: css content
	@cd $(this_dir) && $(pipenv_prefix) hugo

### Clean

clean:
	@rm -rf $(output_dir)/* $(output_dir)/..?* $(output_dir)/.[!.]*

### Audit

audit: audit-sitemap audit-dirs audit-files

audit-sitemap:
	@echo "---] AUDIT SITEMAP [--------------------------------------------------------------"
	@grep '<loc>' $(output_dir)/sitemap.xml | sed 's: *</*loc>::g' | sort -u

audit-dirs:
	@echo "---] AUDIT DIRS [-----------------------------------------------------------------"
	@find $(output_dir) -type d | sed "s:$(output_dir):D :g" | grep -v '^D *$$' | sort -u

audit-files:
	@echo "---] AUDIT FILES [----------------------------------------------------------------"
	@find $(output_dir) -type f | sed "s:$(output_dir):F :g" | sort -u
