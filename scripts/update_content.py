#!/usr/bin/env python3

def parse_file(filename):
    # Import BS version
    bs = None
    try:
        import BeautifulSoup
        from BeautifulSoup import Tag, NavigableString
        bs = BeautifulSoup
    except ImportError:
        import bs4
        from bs4 import Tag, NavigableString
        bs = bs4

    # Patch BS
    def _tag_clone(self):
        copy = type(self)(None, self.builder, self.name, self.namespace, 
                          self.nsprefix)
        # work around bug where there is no builder set
        # https://bugs.launchpad.net/beautifulsoup/+bug/1307471
        copy.attrs = dict(self.attrs)
        for attr in ('can_be_empty_element', 'hidden'):
            setattr(copy, attr, getattr(self, attr))
        for child in self.contents:
            copy.append(child.clone())
        return copy

    Tag.clone = _tag_clone
    NavigableString.clone = lambda self: type(self)(self)

    # Parse
    with open(filename, 'r') as fh:
        html = fh.read()

    parsed_html = bs.BeautifulSoup(html)
    return (parsed_html, bs)


def extract_meta(parsed, alternatives, versions):
    data = {
        'tags': [],
        'author': [],
        'resources': [],
        'versions': [],
    }

    # for child in parsed.head:
    #     print(dir(child))
    #     break

    # Metadata from parsed HTML
    for child in parsed.head:
        if child.name == 'title':
            data['title'] = child.text
        elif child.name == 'meta':
            name = child.get('name')
            if name == 'keyword':
                data['tags'].append(child.get('content'))
            elif name == 'author':
                data['author'].append(child.get('content'))
            elif name == 'ietf.draft':
                data['name'] = child.get('content')
                name, version = data['name'].rsplit('-', 1)
                data['version'] = version
            else:
                print('Ignored meta:', child.get('name'), '=', child.get('content'))
        elif child.name == 'script':
            pass # Ignore
        elif child.name in (None, 'style', 'link'):
            pass # Ignore
        else:
            print('========', child.name, '========')
            print(child)

    # Published date
    date = parsed.body.find('time', {'class': 'published'})
    if date is not None:
        date = date.get('datetime')
    if date is not None:
        data['date'] = date

    # Description
    desc = parsed.body.find('section', {'id': 'section-abstract'})
    desc = desc.clone()
    dummy = desc.find('span', {'id': 'publication-notice'})
    if dummy is not None:
        dummy.extract()
    dummy = desc.find('h2', {'id': 'abstract'})
    if dummy is not None:
        dummy.extract()
    data['description'] = desc.get_text(separator=' ')
    data['description'] = data['description'].replace('\n', ' ').replace('\xB6', '').strip()

    # Alternatives
    import os.path
    for ftype, entry in alternatives.items():
        if ftype in ('md', 'html'):
            continue # Ignore

        data['resources'].append({
            'name': ftype,
            'src': os.path.basename(entry['target']),
        })

    # Other versions
    for entry in versions:
        data['versions'].append({
            'label': entry['version'],
            'ref': entry['vname'],
        })

    return data


def write_front_matter(handle, data):
    meta = data.copy()

    # Prepare tags - make unique and add standard tags
    meta['tags'] = set(meta['tags'])
    meta['tags'].add('internet-draft')
    meta['tags'].add('I-D')
    meta['tags'].add('rfc')
    meta['tags'].add('specification')
    meta['tags'].add('specs')
    meta['tags'] = list(meta['tags'])

    # Template selection
    meta['type'] = 'rfc'

    import yaml

    handle.write('---\n')
    handle.write(yaml.dump(meta, indent=2, default_flow_style=False))
    handle.write('---\n')


def process_xml(source, target):
    import os
    with open(source, 'r') as rh:
        os.makedirs(os.path.dirname(target), exist_ok=True)
        with open(target, 'w') as wh:
            for line in rh.readlines():
                line = line.replace('"rfc2629.xslt"', '"/rfc2629.xslt"')
                wh.write(line)


def process_html(filename, dest, alternatives, versions, canonical):
    # Parse file
    parsed, bs = parse_file(filename)
    meta = extract_meta(parsed, alternatives, versions)

    # Set canonical or alias metadata
    import os
    if canonical is not None:
        meta['canonical'] = os.path.dirname(canonical).replace('content/', '')
    else:
        meta['aliases'] = [
            os.path.dirname(os.path.dirname(dest)).replace('content', '')
        ]

    # Find and extract toc
    toc = parsed.body.find('div', {'id': 'toc'})
    if toc is not None:
        toc = toc.extract()
    meta['toc'] = str(toc)

    # Find and extract info card
    info = parsed.body.find('div', {'id': 'internal-metadata'})
    if info is not None:
        ino = info.extract()
    meta['info'] = str(info)

    # Delete all script elements
    for script in parsed.body.find_all('script'):
        script.decompose()

    # Create output file
    with open(dest, 'w') as fh:
        write_front_matter(fh, meta);

        for child in parsed.body.children:
            fh.write(str(child))

    return meta


def collect_versions(prefix, drafts, specs_dir, content_dir):
    import os
    with os.scandir(prefix) as it:
        for entry in it:
            if entry.name[0] == '.':
                continue
            if entry.name in ('index.html', 'current.md'):
                continue

            # Name + version and extension
            import os.path
            vname, *ext = entry.name.split('.')
            ext = '.'.join(ext)

            # Version - ends with -XX
            name, version = vname.rsplit('-', 1)

            # Assemble
            if name not in drafts:
                drafts[name] = {}

            ver = int(version)
            if ver not in drafts[name]:
                drafts[name][ver] = {
                    'version': version,
                    'vname': vname,
                    'files': {},
                }

            basedir = os.path.join(content_dir, name)
            verdir = os.path.join(basedir, vname)

            if ext == 'html':
                target = os.path.join(verdir, 'index.html')
            else:
                target = os.path.join(verdir, os.path.basename(entry.path))

            drafts[name][ver]['files'][ext] = {
                'source': entry,
                'target': target,
            }


def process_draft(prefix, basename, versions):
    # Basedir
    import os.path

    # For every version, create its own directory
    latest_meta = None
    for version in reversed(versions):
        vname = version['vname']
        print(f'>>> Processing "{vname}"...')

        # For the version, copy each file verbatim *except* for the HTML file.
        # That needs further processing.
        for ftype, entry in version['files'].items():
            source = entry['source'].path
            target = entry['target']

            os.makedirs(os.path.dirname(target), exist_ok=True)

            if ftype == 'md':
                pass # Ignore
            elif ftype.endswith('xml'):
                print(f'... Processing "{source}" to "{target}".')
                process_xml(source, target)
            elif ftype != 'html':
                print(f'... Copying "{source}" to "{target}".')
                import shutil
                shutil.copy(source, target)
            else:
                print(f'... Processing "{source}" to "{target}"...')
                canonical = None
                if latest_meta is not None:
                    canonical = latest_meta['_canonical']

                meta = process_html(source, target, version['files'], versions, canonical)

                if latest_meta is None:
                    latest_meta = meta
                    latest_meta['_canonical'] = target

    # Redirect to latest version from /draft/ url
    target = os.path.join(prefix, basename, '_index.md')
    # FIXME this fails on new RFCs, fix later
    index_meta = latest_meta.copy()
    index_meta['type'] = 'rfc'
    index_meta['layout'] = 'redirect'
    index_meta['redirect_ref'] = index_meta['name']
    with open(target, 'w') as fh:
        import yaml

        fh.write('---\n')
        fh.write(yaml.dump(index_meta, indent=2, default_flow_style=False))
        fh.write('---\n')

    return (basename, latest_meta)


def process_all():
    import os

    drafts = {}
    with os.scandir('specs') as it:
        for x in it:
            if x.name.startswith('draft-'):
                collect_versions(x, drafts, 'specs', 'content')

    processed = []
    for draft, data in drafts.items():
        listed = [v for k, v in sorted(data.items())]

        ret = process_draft('content', draft, listed)
        processed.append(ret)

    print(f'Processed {len(processed)} drafts. Now generating index...')

    index = os.path.join('content', '_index.md')
    with open(index, 'w') as fh:
        fh.write('''---
type: rfc
layout: list
title: "Interpeer Project Specifications"
---
''')


if __name__ == '__main__':
    process_all()
