#!/bin/sh

INDEX_NOW_KEY=0a67571dff854469bab703e0de32746c
SITEMAP=https://specs.interpeer.io/sitemap.xml
URLS="
https://www.bing.com/indexnow?url=@URL@&key=@INDEX_NOW_KEY@
https://api.indexnow.org/indexnow?url=@URL@&key=@INDEX_NOW_KEY@
https://search.seznam.cz/indexnow?url=@URL@&key=@INDEX_NOW_KEY@
https://yandex.com/indexnow?url=@URL@&key=@INDEX_NOW_KEY@
http://www.google.com/webmasters/sitemaps/ping?sitemap=@URL@
http://www.google.com/webmasters/tools/ping?sitemap=@URL@
"

sed_sitemap="$(echo "$SITEMAP" | sed 's:/:\\/:g')"

for url in $URLS ; do
  if [ "$url" = "" ] ; then
    continue
  fi
  full_url="$(echo "$url" | sed "s/@URL@/$sed_sitemap/g")"
  full_url="$(echo "$full_url" | sed "s/@INDEX_NOW_KEY@/$INDEX_NOW_KEY/g")"
  echo ">>> Pinging: $full_url"
  curl -LG --fail-with-body "$full_url" | lynx -dump -force_html -stdin
  echo "\n"
done
